+++
title = "Getting Started With Sysmac Studio"
date = "2021-01-15T15:06:09+05:30"
author = "KD"
authorTwitter = "kdpuvvadi" #do not include @
cover = ""
tags = ["", ""]
keywords = ["", ""]
description = ""
showFullContent = false
draft = true
+++

## Sysmac Studio

Omron's Process Automation Intigrated Development evironment. I've known omron my whole career and been using their products for various use cases. Prviously CX-One was their IDE for all their products and they lagged behind industry competers. Then, they released NJ/NX Series Motion Controllers, 1S Servo Drive, FH vision systems etc. Sysmac Studio is actively under development and getting continues updates. Anyone who would like to try IDE before going in to the ecosystem, they should first Download the trial, try the platform and get to know the ecosystem.

## Download Sysmac Studio

Sysmac stduio can be downloaded with below link. Fill the form and you'll receive the email with download links to both full vesion if you've already purchased the product and trail version with key for 30 Days eveolution period.

****[Download Sysmac Studio from Omron Automation](https://automation.omron.com/en/us/forms/download-sysmac-studio-standard-edition)****

## Installation

After the downnload completed, Extract the iso with zip archive of your choice or you can simply mount. Please keep in mind that to install version 1.4 or later you need Windows 10 x64 pc. If you need to install it on the 32bit operating system, you need to go for 1.3 or less.

## What is Process Automation

Process Automation/Industrial Automation
