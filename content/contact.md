---
title: "Contact"
---

<!-- markdownlint-disable -->

{{< rawhtml >}}
  <link rel="stylesheet" href="/css/form.css">

  <script type="text/javascript">var submitted=false;</script>
  
  <iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" 
  onload="if(submitted) {window.location='/thankyou';}"></iframe>

  <form action="https://docs.google.com/forms/d/e/18IqVKDW2Og3_-jCCzW4SOk2vpa_FHFC_48lrO-UDGOE/formResponse"
  method="post" target="hidden_iframe" onsubmit="submitted=true;">
  </form>

  <form action="https://docs.google.com/forms/d/e/1FAIpQLSdBit8IwBmMxXARhjnvMkn4M4fPFpTQSjDaZEo6CAAh8FoltQ/formResponse" method="post" target="hidden_iframe" onsubmit="submitted=true" netlify >
  <label>Name*</label>
    <input type="text" placeholder="Name*" class="form-input" name="entry.149128287" required>
  <label>Email*</label>
    <input type="email" placeholder="Email*" class="form-input" name="entry.630496343" required>

  <label>Subject*</label>
    <input type="text" placeholder="Subject*" class="form-input" name="entry.399879000" required>
    
  <label>Message</label>
    <textarea rows="5" placeholder="Enter your Message" class="form-input" name="entry.1494840321" ></textarea>
    
  <button type="submit">Send</button>
  </form>
{{< /rawhtml >}}
